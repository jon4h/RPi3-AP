#!/bin/bash
sudo -s

apt install -y dnsmasq hostapd

echo "\ndenyinterfaces wlan0" >> /etc/dhcpcd.conf

mv /etc/network/interfaces /etc/network/interfaces.orig

echo "source-directory /etc/network/interfaces.d\n\n" >> /etc/network/interfaces
echo "auto lo\n" >> /etc/network/interfaces
echo "iface lo inet loopback\n\n" >> /etc/network/interfaces
echo "iface eth0 inet manual\n\n" >> /etc/network/interfaces
echo "allow-hotplug wlan0\n" >> /etc/network/interfaces
echo "iface wlan0 inet static\n" >> /etc/network/interfaces
echo "\tadress 172.24.1.1\n" >> /etc/network/interfaces
echo "\tnetmask 255.255.255.0" >> /etc/network/interfaces
echo "\tnetwork 172.24.1.0\n" >> /etc/network/interfaces
echo "\tbroadcasr 172.24.1.255\n\n" >> /etc/network/interfaces
echo "allow-hotplug wlan1\n" >> /etc/network/interfaces
echo "iface wlan1 inet manual\n" >> /etc/network/interfaces
echo "wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf" >> /etc/network/interfaces

if [ -f /etc/wpa_supplicant/wpa_supplicant.conf ] 
then
	mv /etc/wpa_supplicant/wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant.conf.orig
fi

echo "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\n" >> /etc/wpa_supplicant/wpa_supplicant.conf
echo "update_config=1" >> /etc/wpa_supplicant/wpa_supplicant.conf

service dhcpcd restart

ifdown wlan0
ifup wlan0

if [ -f /etc/hostapd/hostapd.conf ]
then
	mv /etc/hostapd/hostapd.conf /etc/hostapd/hostapd.conf.orig
fi

echo "# This is the name of the WiFi interface we configured above\n" >> /etc/hostapd/hostapd.conf
echo "interface=wlan0\n\n\n" >> /etc/hostapd/hostapd.conf
echo "# Use the nl80211 driver with the brcmfmac driver\n" >> /etc/hostapd/hostapd.conf
echo "driver=nl80211\n\n\n" >> /etc/hostapd/hostapd.conf
echo "# This is the name of the network\n" >> /etc/hostapd/hostapd.conf
read -p "ssid: " ssid
echo "ssid=$ssid\n\n\n" >> /etc/hostapd/hostapd.conf
echo "# Use the 2.4GHz band\n" >> /etc/hostapd/hostapd.conf
echo "hw_mode=g\n\n\n" >> /etc/hostapd/hostapd.conf
echo "# Use channel 6\n" >> /etc/hostapd/hostapd.conf
echo "channel=6\n\n\n" >> /etc/hostapd/hostapd.conf
echo "# Enable 802.11n\n" >> /etc/hostapd/hostapd.conf
echo "ieee80211n=1\n\n\n" >> /etc/hostapd/hostapd.conf
echo "# Enable WMM\n" >> /etc/hostapd/hostapd.conf
echo "wmm_enabled=1\n\n\n" >> /etc/hostapd/hostapd.conf
echo "# Enable 40MHz channels with 20ns guard interval\n" >> /etc/hostapd/hostapd.conf
echo "ht_capab=[HT40][SHORT-GI-20][DSSS_CCK-40]\n\n\n" >> /etc/hostapd/hostapd.conf
echo "# Accept all MAC addresses\n" >> /etc/hostapd/hostapd.conf
echo "macaddr_acl=0\n\n\n" >> /etc/hostapd/hostapd.conf
echo "# Use WPA authentication\n" >> /etc/hostapd/hostapd.conf
echo "auth_algs=1\n\n\n" >> /etc/hostapd/hostapd.conf
echo "# Require clients to know the network name\n" >> /etc/hostapd/hostapd.conf
echo "ignore_broadcast_ssid=0\n\n\n" >> /etc/hostapd/hostapd.conf
echo "# Use WPA2\n" >> /etc/hostapd/hostapd.conf
echo "wpa=2\n\n\n" >> /etc/hostapd/hostapd.conf
echo "# Use a pre-shared key\n" >> /etc/hostapd/hostapd.conf
echo "wpa_key_mgmt=WPA-PSK\n" >> /etc/hostapd/hostapd.conf
echo "# The network passphrase\n" >> /etc/hostapd/hostapd.conf
read -sp "Passwort: " Passwort
echo "wpa_passphrase=$Passwort\n\n\n" >> /etc/hostapd/hostapd.conf
echo "# Use AES, instead of TKIP\n" >> /etc/hostapd/hostapd.conf
echo "rsn_pairwise=CCMP\n" >> /etc/hostapd/hostapd.conf

sed -i.bak 's/#DAEMON_CONF=""/DAEMON_CONF="\/etc\/hostapd\/hostapd.conf"/g' /etc/default/hostapd


echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf


sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"

iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
iptables -A FORWARD -i $INC -o $WIFI -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i $WIFI -o $INC -j ACCEPT

awk '/iptables-restore < /etc/iptables.ipv4.nat/{print "exit0"}1' /etc/rc.local


service hostapd start  
service dnsmasq start

/usr/sbin/hostapd /etc/hostapd/hostapd.conf
